#Name: Export ArcGIS Server Map Service Layer to Shapefile
#Author: Bryan McIntosh
import urllib2, os, arcpy

# Variables
myUrl = "https://sampleserver6.arcgisonline.com/arcgis/rest/services"
myService = "/Census/MapServer"
myParams = "/3/query?where=1%3D1&geometryType=esriGeometryEnvelope&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=*&returnGeometry=true&geometryPrecision=&outSR=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&returnZ=false&returnM=false&returnDistinctValues=false&returnTrueCurves=false&f=pjson"

# Query ArcGIS Server Map Service
myRequest = myUrl + myService + myParams
response = urllib2.urlopen(myRequest)
myJSON = response.read()

# Write response to json text file
foo = open("jsonOutput.json", "wb")
foo.write(myJSON);
foo.close()

# Create Feature Class
ws = os.getcwd() + os.sep
arcpy.JSONToFeatures_conversion("jsonOutput.json", ws + "finalShapfile.shp")