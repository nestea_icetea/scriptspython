from PyPDF2 import PdfMerger
from os import path
from pathlib import Path
from glob import glob

def find_ext(ext):
  return glob(path.join("*.{}".format(ext)))

list_of_pdfs = find_ext("pdf")
list_of_pdfs = list(map(Path, list_of_pdfs))
list_to_merge = {}

for i in list_of_pdfs:
  list_to_merge[i] = []
  for file in list_of_pdfs:
      if i.name.split(".")[0] in file.name.split(".")[0]:
        list_to_merge[i].append(file)

for i in list_to_merge:
  merger = PdfMerger()
  if len(list_to_merge[i]) > 1:
    for pdf in list_to_merge[i]:
      merger.append(pdf)
  merger.write(i.name)
  merger.close()
