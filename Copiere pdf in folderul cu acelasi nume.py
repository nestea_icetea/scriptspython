#1.Acest script parcurge un folder (folder1) ce contine mai multe .pdf-uri
#2.Pentru fiecare pdf din folder1, extrage denumirea si creeaza calea catre locatia din folder2
#3.Copiaza fisierele .pdf din folder1 in folderul2


import os
import shutil


folder1 = "Z:\\WRM\\38_EXPROPRIERI_JIU\\00_CERERE OCPI\CERERE EXTRASE OCPI\\test2" #calea catre folderul cu pdf-uri
folder2 = "Z:\\WRM\\38_EXPROPRIERI_JIU\\00_CERERE OCPI\CERERE EXTRASE OCPI\\test" #calea catre locatia cu folderele aferente pdf-urilor
files=[] #lista cu toate fisierele .pdf din folder

#parcurgerea folderului si salvarea denumirilor in lista f
files = [os.path.splitext(filename)[0] for filename in os.listdir(folder1)]

for i in files:
    src_file = folder1 + "\\" + str(i) + ".pdf" #construieste calea fisierului .pdf
    dst_file = folder2 +"\\" + str(i) + "\\"    #construieste calea catre folder
    shutil.copy(src_file, dst_file)             #muta pdf-ul in folderul corespunzator


